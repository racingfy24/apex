create or replace PROCEDURE put_file
 (p_base_url          IN VARCHAR2,
  p_bucket_name       IN VARCHAR2,
  p_file_name         IN VARCHAR2,
  p_authentication    IN VARCHAR2) IS

  l_response          CLOB;
  l_object_store_url  VARCHAR2(1000);
  l_request_filename  VARCHAR2(1000);
  l_extension         VARCHAR2(100);
  l_request_object	  BLOB;
  l_unzipped_file     BLOB;
  l_files             apex_zip.t_files;

  PROCEDURE upload_file(p_request_filename  VARCHAR2,
                        p_file              BLOB) IS

  BEGIN
      -- Build the full URL to the document. 
      l_object_store_url := p_base_url||'/b/'||p_bucket_name||'/o/'||p_request_filename;
      
      -- Call Web Service to PUT file in OCI.
      l_response := apex_web_service.make_rest_request
       (p_url                  => UTL_URL.ESCAPE(l_object_store_url),
        p_http_method          => 'PUT',
        p_body_blob            => p_file,
        p_credential_static_id => p_authentication);

      IF apex_web_service.g_status_code != 200 then
        raise_application_error(-20111,'Unable to Upload File.');
      END IF;

  END upload_file;

BEGIN

  select blob_content, 
         regexp_replace( filename, '[0-9\/^]', null ), 
         regexp_substr(filename, '\.[^\.]*$')
  into l_request_object, l_request_filename, l_extension
  from apex_application_temp_files 
  where name = p_file_name;

  apex_debug.message('EXTENSION: %s', lower(l_extension));

  IF(lower(l_extension) = '.zip')THEN

    l_files := apex_zip.get_files (
            p_zipped_blob => l_request_object);

    for i in 1 .. l_files.count loop
        l_unzipped_file := apex_zip.get_file_content (
            p_zipped_blob => l_request_object,
            p_file_name   => l_files(i) );

        upload_file(p_request_filename => l_files(i), 
                    p_file => l_unzipped_file);

    end loop;

  ELSE
     upload_file(p_request_filename => l_request_filename, 
                 p_file => l_request_object);
     
  END IF;

EXCEPTION 
   WHEN OTHERS THEN 
       raise_application_error(-20111,'Error to Upload File:'||p_file_name||'  SQLCODE => '||SQLCODE||'--'||dbms_utility.format_error_backtrace);
END put_file;
/
