BEGIN
  DBMS_NETWORK_ACL_ADMIN.APPEND_HOST_ACE (
    HOST         => 'api.openai.com',
    LOWER_PORT   => 443,
    UPPER_PORT   => 443,
    ACE          => xs$ace_type(
        PRIVILEGE_LIST => xs$name_list('http'),
        PRINCIPAL_NAME => 'ADMIN',
        PRINCIPAL_TYPE => xs_acl.ptype_db));
END;
/
BEGIN
  --DBMS_CLOUD.DROP_CREDENTIAL (credential_name  => 'OPENAI_CRED');
  DBMS_CLOUD.CREATE_CREDENTIAL(
    credential_name => 'OPENAI_CRED',
    username => 'OPENAI',
    password => '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' );
END;
/    
BEGIN
  --DBMS_CLOUD_AI.drop_profile(profile_name => 'OPENAI');
  DBMS_CLOUD_AI.create_profile(
      profile_name => 'OPENAI',
      attributes => '{"provider": "openai",
                      "credential_name": "OPENAI_CRED",
                      "object_list": [{"owner": "WKSP_RACING", "name": "TRANSCRIBE_AUDIO"}]
       }');

  DBMS_CLOUD_AI.SET_PROFILE(profile_name => 'OPENAI');
END;
/
